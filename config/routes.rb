Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get 'home/index'
  root 'home#index'

  get 'calculate/:amount', to: 'calculations#calculate', constraints: { :amount => /[^\/]+/ }, defaults: { format: 'json' }

  resources :calculations, only: [:create, :show]

end
