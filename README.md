# ¢oin
Accessable on the web at [get-that-coin.herokuapp.com](https://get-that-coin.herokuapp.com)

## Getting Started
* Rails Version
5.0.2

* Ruby version
2.3.3

### To run the app locally:
  1. run db:setup
  2. start the server with `rails s`
  3. navigate to `localhost:3000`

if you'd like to run a GET request for calculations via JSON:
```
GET /calculate/#{amount}
```
Or you can POST a calculation to:
```
POST /calculations.json?calculation[amount]=#{amount}
```

* Database initialization
```
rails db:setup
```

* How to run the test suite
```
rails spec
```

