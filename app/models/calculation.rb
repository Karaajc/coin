class Calculation < ApplicationRecord
  MONEY_PATTERN = /\${1}(.\d*)\.(\d{2})/
  DENOMS = {
    silver_dollars: 100,
    half_dollars: 50,
    quarters: 25,
    dimes: 10,
    nickels: 5,
    pennies: 1
    }


  def convert_to_change
    self.amount.gsub!(MONEY_PATTERN,'\1\2')
  end

  def convert_to_dollas
    self.amount.insert(-3, ".").to_f
  end

  def enough_money?
    self.amount.to_i > 0
  end

  def get_that_coin
    change = self.amount.to_i
    results = self
    DENOMS.map do |coin|
      split = change.divmod(coin[1])
      results["#{coin[0]}"] = split[0] #calculation
      change = split[1]
    end
    results
  end



end
