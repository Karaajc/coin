class CalculationsController < ApplicationController

  def show
    @calculation = Calculation.find(params[:id])
  end

  def create
    @calculation = Calculation.new(calculation_params)
    @calculation.convert_to_change
    # run get_that_coin on amount
    @calculation.get_that_coin

    respond_to do |format|
      if @calculation.save
        format.html { redirect_to @calculation, notice: 'Voila! your coin has been calculated!' }
        format.json { render json: @calculation }
      else
        format.html { render 'home/index', @calculation.errors}
        format.json { render json: @calculation.errors, status: :unprocessable_entity }
      end
    end
  end

  def calculate
    params.permit!
    @calculation = Calculation.new({amount: params[:amount]})
    @calculation.convert_to_change

    if @calculation.enough_money?
      @calculation.get_that_coin
    else
      render json: @calculation.errors, status: :unprocessable_entity
    end
  end

  private

  def calculation_params
      params.require(:calculation).permit(:amount)
  end


end
