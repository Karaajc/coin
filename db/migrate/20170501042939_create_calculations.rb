class CreateCalculations < ActiveRecord::Migration[5.0]
  def change
    create_table :calculations do |t|
      t.string      :amount,         null: false
      t.integer     :silver_dollars, default: 0
      t.integer     :half_dollars,   default: 0
      t.integer     :quarters,       default: 0
      t.integer     :dimes,          default: 0
      t.integer     :nickels,        default: 0
      t.integer     :pennies,        default: 0
      t.timestamps
    end
  end
end
