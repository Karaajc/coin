require 'rails_helper'

RSpec.describe Calculation, type: :model do
  it { should allow_value('$0.99').for(:amount)}
  it { should allow_value('$1.56').for(:amount)}

end
